import matplotlib.pyplot as plt
import numpy as np
import operator

with open('Tput_Lte0') as f:
	lines = f.read().splitlines()

Tput_lte = map(float, lines);

with open('Tput_Wifi0') as f:
	lines2 = f.read().splitlines()

Tput_wifi = map(float, lines2);

t = np.arange(0, 20.0, 0.04)

for i in range(len(lines),len(t),1):
	Tput_lte.append(0)
for i in range(len(lines2),len(t),1):
	Tput_wifi.append(0)

Tput_agregado = map(operator.add, Tput_lte,Tput_wifi)

print len(Tput_lte)
print len(Tput_wifi)
print len(t)

plt.plot(t,Tput_lte, label="LTE", linewidth=1.5)
plt.plot(t,Tput_wifi, label="WIFI", linewidth=1.5, color='r')
plt.plot(t,Tput_agregado, label="Agregado (LTE+WIFI)", linewidth=1.5, color='green',)
plt.legend(loc=1)
plt.grid(color='black', linestyle='--', linewidth=0.2)
plt.ylim([0,150])
plt.xlabel('Tempo (s)')
plt.ylabel(u'Taxa Oferecida (Mbps)')
plt.show()

#plt.plot(lines)
#plt.show()
